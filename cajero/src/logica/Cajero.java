package logica;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

/**
 * Clase cajero que ejecuta el menu
 * 
 * @author cass465
 * @version 23/03/2018
 */
public class Cajero implements CajeroInterface {
	CuentaAhorros cuentasAhorros[] = new CuentaAhorros[10]; // arreglo que contiene cuentas de ahorros
	CuentaCorriente cuentasCorrientes[] = new CuentaCorriente[10]; // arreglo que contiene cuentas corrientes
	byte tipoCuenta;
	long numCuenta;
	long deposito;
	long retiro;

	/**
	 * menu del cajero
	 */
	public void menuPrincipal() {
		@SuppressWarnings("resource") // eliminar warnings
		Scanner leer = new Scanner(System.in);
		byte opcion;
		crearObjetos();
		do {
			System.out.println("--MENU PRINCIPAL--");
			System.out.println("1. CREAR PERSONA");
			System.out.println("2. LISTAR USUARIOS");
			System.out.println("3. CONSIGNAR");
			System.out.println("4. RETIRAR");
			System.out.println("5. CONSULTAR SALDO");
			System.out.println("0. SALIR");
			opcion = leer.nextByte();
			switch (opcion) {
			case 1:
				crearPersona();
				break;
			case 2:
				listarUsuarios();
				break;
			case 3:
				consignar();
				break;
			case 4:
				retirar();
				break;
			case 5:
				consultarSaldo();
				break;
			case 0:
				System.out.println("--EL PROGRAMA HA FINALIZADO--");
				break;
			default:
				System.out.println("OPCION INVALIDA!!");
			}
		} while (opcion != 0);

	}

	/**
	 * crear las cuentas
	 */
	public void crearObjetos() {
		// arreglo de cuentas de ahorros
		this.cuentasAhorros[0] = new CuentaAhorros("CUENTA DE AHORROS", "YEISON CIFUENTES", 461217161, 6_000_000);
		this.cuentasAhorros[1] = new CuentaAhorros("CUENTA DE AHORROS", "ARTURO CAMPOS", 461217162, 500_000);
		this.cuentasAhorros[2] = new CuentaAhorros("CUENTA DE AHORROS", "JULIANA MEDINA", 461217163, 600_000);
		this.cuentasAhorros[3] = new CuentaAhorros("CUENTA DE AHORROS", "ANDRES VARGAS", 461217164, 900_000);
		this.cuentasAhorros[4] = new CuentaAhorros("CUENTA DE AHORROS", "LUIS MOTOA", 461217165, 1_000_000);
		// arreglo de cuentas corrientes
		this.cuentasCorrientes[0] = new CuentaCorriente("CUENTA CORRIENTE", "CAMILO SANABRIA", 461217172, 6_000_000);
		this.cuentasCorrientes[1] = new CuentaCorriente("CUENTA CORRIENTE", "CARLOS RAMIREZ", 461217173, 400_000);
		this.cuentasCorrientes[2] = new CuentaCorriente("CUENTA CORRIENTE", "JUAN BENAVIDES", 461217174, 700_000);
		this.cuentasCorrientes[3] = new CuentaCorriente("CUENTA CORRIENTE", "LAURA LEON", 461217175, 5_000_000);
		this.cuentasCorrientes[4] = new CuentaCorriente("CUENTA CORRIENTE", "FELIPE BARRAGAN", 461217176, 300_000);
	}

	/**
	 * crear persona o cuenta
	 */
	@Override
	public void crearPersona() {
		// TODO Auto-generated method stub
		@SuppressWarnings("resource")
		Scanner leer = new Scanner(System.in);
		byte opcion;
		String nombreTitular;
		int numeroCuenta;
		long saldo;
		byte cupoCuentasAhorros = 0;
		byte cupoCuentasCorrientes = 0;
		System.out.println("QUE TIPO DE CUENTA DESEA CREAR");
		System.out.println("1. CUENTA DE AHORROS");
		System.out.println("2. CUENTA CORRIENTE");
		opcion = leer.nextByte();
		switch (opcion) {
		case 1:
			for (int i = 0; i < 10; i++) {
				if (cuentasAhorros[i] != null) {
					cupoCuentasAhorros++;
				}

			}
			if (cupoCuentasAhorros < 10) {
				for (int i = 0; i < 10; i++) {
					if (cuentasAhorros[i] == null) {
						System.out.print("INGRESE EL NOMBRE DEL TITULAR: ");
						nombreTitular = leer.nextLine();
						nombreTitular = leer.nextLine();
						System.out.print("INGRESE EL NUMERO DE CUENTA: ");
						numeroCuenta = leer.nextInt();
						for (int j = 0; cuentasCorrientes[j] != null; j++) {
							while (cuentasCorrientes[j].getNumeroCuenta() == numeroCuenta) {
								System.out.print("ESTE NUMERO YA SE ENCUENTRA REGISTRADO, INTENTE NUEVAMENTE");
								numeroCuenta = leer.nextInt();
							}
						}
						for (int j = 0; cuentasAhorros[j] != null; j++) {
							while (cuentasAhorros[j].getNumeroCuenta() == numeroCuenta) {
								System.out.print("ESTE NUMERO YA SE ENCUENTRA REGISTRADO, INTENTE NUEVAMENTE");
								numeroCuenta = leer.nextInt();
							}
						}
						System.out.print("INGRESE EL SALDO INICIAL DE LA CUENTA: ");
						saldo = leer.nextLong();
						while (saldo < 0) {
							System.out.print("SALDO INVALIDO!!");
							saldo = leer.nextLong();
						}
						cuentasAhorros[i] = new CuentaAhorros("CUENTA DE AHORROS", nombreTitular.toUpperCase(),
								numeroCuenta, saldo);

						i = 9;
					}
				}
			} else {
				System.out.println("--------------------------------------------");
				System.out.println("EL CUPO PARA ESTE TIPO DE CUENTAS ESTA LLENO");
				System.out.println("--------------------------------------------");
			}
			break;
		case 2:
			for (int i = 0; i < 10; i++) {
				if (cuentasCorrientes[i] != null) {
					cupoCuentasCorrientes++;
				}

			}
			if (cupoCuentasCorrientes < 10) {
				for (int i = 0; i < 10; i++) {
					if (cuentasCorrientes[i] == null) {
						System.out.print("INGRESE EL NOMBRE DEL TITULAR: ");
						nombreTitular = leer.nextLine();
						nombreTitular = leer.nextLine();
						System.out.print("INGRESE EL NUMERO DE CUENTA: ");
						numeroCuenta = leer.nextInt();
						for (int j = 0; cuentasCorrientes[j] != null; j++) {
							while (cuentasCorrientes[j].getNumeroCuenta() == numeroCuenta) {
								System.out.print("ESTE NUMERO YA SE ENCUENTRA REGISTRADO, INTENTE NUEVAMENTE");
								numeroCuenta = leer.nextInt();
							}
						}
						for (int j = 0; cuentasAhorros[j] != null; j++) {
							while (cuentasAhorros[j].getNumeroCuenta() == numeroCuenta) {
								System.out.print("ESTE NUMERO YA SE ENCUENTRA REGISTRADO, INTENTE NUEVAMENTE");
								numeroCuenta = leer.nextInt();
							}
						}
						System.out.print("INGRESE EL SALDO INICIAL DE LA CUENTA: ");
						saldo = leer.nextLong();
						while (saldo < 0) {
							System.out.print("SALDO INVALIDO!!");
							saldo = leer.nextLong();
						}
						cuentasCorrientes[i] = new CuentaCorriente("CUENTA CORRIENTE", nombreTitular.toUpperCase(),
								numeroCuenta, saldo);

						i = 9;
					}
				}
			} else {
				System.out.println("--------------------------------------------");
				System.out.println("EL CUPO PARA ESTE TIPO DE CUENTAS ESTA LLENO");
				System.out.println("--------------------------------------------");
			}
			break;
		default:
			System.out.println("--OPCION INVALIDA--");
		}
	}

	/**
	 * mostrar las cuentas
	 */
	@Override
	public void listarUsuarios() {
		@SuppressWarnings("resource")
		Scanner leer = new Scanner(System.in);
		byte opcion;
		System.out.println("1. VER CUENTAS DE AHORROS");
		System.out.println("2. VER CUENTAS CORRIENTES");
		opcion = leer.nextByte();
		while (opcion != 1 && opcion != 2) {
			System.out.println("OPCION INVALIDA!!");
			opcion = leer.nextByte();
		}
		try {
			try (ObjectOutputStream escritura = new ObjectOutputStream(new FileOutputStream("usuariosCajero.txt"))) {
				escritura.writeObject(this.cuentasAhorros);
				escritura.writeObject(this.cuentasCorrientes);
				escritura.close();
			}
			try (ObjectInputStream lectura = new ObjectInputStream(new FileInputStream("usuariosCajero.txt"))) {
				CuentaAhorros cuentasAhorrosLectura[] = (CuentaAhorros[]) lectura.readObject();
				CuentaCorriente cuentasCorrientesLectura[] = (CuentaCorriente[]) lectura.readObject();
				lectura.close();
				if (opcion == 1) {
					for (int i = 0; i < cuentasAhorrosLectura.length; i++) {
						if (cuentasAhorrosLectura[i] != null) {
							System.out.println((i + 1) + ". " + cuentasAhorrosLectura[i]);
						}
					}
				}
				if (opcion == 2) {
					for (int i = 0; i < cuentasCorrientesLectura.length; i++) {
						if (cuentasCorrientesLectura[i] != null) {
							System.out.println((i + 1) + ". " + cuentasCorrientesLectura[i]);
						}
					}
				}

			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (IOException e) {

		}
		// TODO Auto-generated method stub

	}

	/**
	 * consignar a una cuenta
	 */
	@Override
	public void consignar() {
		boolean existeCuenta = false;
		@SuppressWarnings("resource")
		Scanner leer = new Scanner(System.in);
		System.out.println("DIGETE EL TIPO DE CUENTA A LA CUAL QUIERE CONSIGNAR");
		System.out.println("1.CUENTA DE AHORROS   2.CUENTA CORRIENTE");
		tipoCuenta = leer.nextByte();
		System.out.println("DIGETE EL NUMERO DE LA CUENTA A LA CUAL VA A CONSIGNAR");
		numCuenta = leer.nextLong();
		switch (tipoCuenta) {

		case 2:
			for (int i = 0; cuentasCorrientes[i] != null; i++) {
				if (cuentasCorrientes[i].getNumeroCuenta() == numCuenta) {
					System.out.println("INGRESE LA CANTIDAD DE DINERO A DEPOSITAR");
					deposito = leer.nextLong();
					while (deposito <= 0) {
						System.out.print("CONSIGNACION INVALIDA!!");
						deposito = leer.nextLong();

					}
					cuentasCorrientes[i].consignar(deposito);
					System.out.println("CONSIGNACION EFECTUADA SATISFACTORIAMENTE");
					System.out.println("TITULAR DE LA CUENTA " + cuentasCorrientes[i].getNombreTitular());
					existeCuenta = true;

				}

			}
			break;
		case 1:
			for (int i = 0; cuentasAhorros[i] != null; i++) {
				if (cuentasAhorros[i].getNumeroCuenta() == numCuenta) {
					System.out.println("INGRESE LA CANTIDAD DE DINERO A CONSIGNAR");
					deposito = leer.nextLong();
					while (deposito <= 0) {
						System.out.print("CONSIGNACION INVALIDA!!");
						deposito = leer.nextLong();

					}
					cuentasAhorros[i].consignar(deposito);
					System.out.println("CONSIGNACION EFECTUADA SATISFACTORIAMENTE");
					System.out.println("TITULAR DE LA CUENTA " + cuentasAhorros[i].getNombreTitular());
					existeCuenta = true;
				}

			}

			break;

		default:
			System.out.println("OPCION INVALIDA");

		}
		if (existeCuenta == false) {
			System.out.println("EL NUMERO DE CUENTA INGRESADO NO EXISTE");
		}

	}

	/**
	 * retirar
	 */
	@Override
	public void retirar() {

		boolean existeCuenta = false;
		@SuppressWarnings("resource")
		Scanner leer = new Scanner(System.in);

		System.out.println("DIGETE EL TIPO DE CUENTA");
		System.out.println("1.CUENTA DE AHORROS   2.CUENTA CORRIENTE");
		tipoCuenta = leer.nextByte();
		System.out.println("DIGETE EL NUMERO DE LA CUENTA DE LA CUAL VA A RETIRAR");
		numCuenta = leer.nextLong();
		switch (tipoCuenta) {

		case 2:
			for (int i = 0; cuentasCorrientes[i] != null; i++) {
				if (cuentasCorrientes[i].getNumeroCuenta() == numCuenta) {
					System.out.println("INGRESE LA CANTIDAD DE DINERO A RETIRAR");
					retiro = leer.nextLong();
					while (retiro <= 0) {
						System.out.print("RETIRO INVALIDO!!");
						retiro = leer.nextLong();
					}
					while (retiro + (retiro * 0.02) > cuentasCorrientes[i].getSaldo()) {
						System.out.println(
								"EL VALOR INGRESADO SUPERA EL VALOR DEL SALDO , INTENTE RETIRAR OTRA CANTIDAD");
						retiro = leer.nextLong();
					}
					cuentasCorrientes[i].retirar(retiro);
					System.out.println("RETIRO EFECTUADO SATISFACTORIAMENTE");
					System.out.println("TITULAR DE LA CUENTA " + cuentasCorrientes[i].getNombreTitular());
					existeCuenta = true;

				}

			}
			break;
		case 1:
			for (int i = 0; cuentasAhorros[i] != null; i++) {
				if (cuentasAhorros[i].getNumeroCuenta() == numCuenta) {
					System.out.println("INGRESE LA CANTIDAD DE DINERO A RETIRAR");
					retiro = leer.nextLong();
					while (retiro <= 0) {
						System.out.print("RETIRO INVALIDO!!");
						retiro = leer.nextLong();
					}
					while (retiro + (retiro * 0.05) > cuentasAhorros[i].getSaldo()) {
						System.out.println(
								"EL VALOR INGRESADO SUPERA EL VALOR DEL SALDO , INTENTE RETIRAR OTRA CANTIDAD");
						retiro = leer.nextLong();
					}
					cuentasAhorros[i].retirar(retiro);
					System.out.println("RETIRO EFECTUADO SATISFACTORIAMENTE");
					System.out.println("TITULAR DE LA CUENTA " + cuentasAhorros[i].getNombreTitular());
					existeCuenta = true;

				}

			}

			break;

		default:
			System.out.println("OPCION INVALIDA");

		}
		if (existeCuenta == false) {
			System.out.println("EL NUMERO DE CUENTA INGRESADO NO EXISTE");
		}

	}

	/**
	 * consultar saldo
	 */
	@Override
	public void consultarSaldo() {
		boolean existeCuenta = false;
		@SuppressWarnings("resource")
		Scanner leer = new Scanner(System.in);
		System.out.println("DIGETE EL TIPO DE SU  CUENTA");
		System.out.println("1.CUENTA DE AHORROS   2.CUENTA CORRIENTE");
		tipoCuenta = leer.nextByte();
		System.out.println("DIGETE EL NUMERO DE SU CUENTA");
		numCuenta = leer.nextLong();

		switch (tipoCuenta) {
		case 2:
			for (int i = 0; cuentasCorrientes[i] != null; i++) {
				if (cuentasCorrientes[i].getNumeroCuenta() == numCuenta) {
					System.out.println("ACTUALMENTE USTED CUENTA CON " + cuentasCorrientes[i].getSaldo() + " $");
					System.out.println("TITULAR DE LA CUENTA " + cuentasCorrientes[i].getNombreTitular());
					existeCuenta = true;

				}

			}
			break;
		case 1:
			for (int i = 0; cuentasAhorros[i] != null; i++) {
				if (cuentasAhorros[i].getNumeroCuenta() == numCuenta) {
					System.out.println("ACTUALMENTE USTED CUENTA CON " + cuentasAhorros[i].getSaldo() + " $");
					System.out.println("TITULAR DE LA CUENTA " + cuentasAhorros[i].getNombreTitular());
					existeCuenta = true;

				}

			}

			break;

		default:
			System.out.println("OPCION INVALIDA");

		}
		if (existeCuenta == false) {
			System.out.println("EL NUMERO DE CUENTA INGRESADO NO EXISTE");
		}

	}

}
