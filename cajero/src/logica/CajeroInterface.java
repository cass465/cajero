package logica;

/**
 * Interface que contiene las opciones del cajero
 * 
 * @author cass465
 * @version 23/03/2018
 */
public interface CajeroInterface {
	void crearPersona();

	void listarUsuarios();

	void consignar();

	void retirar();

	void consultarSaldo();
}
