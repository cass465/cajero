package logica;

import java.io.Serializable;

/**
 * clase que contiene las caracteristicas de una cuenta bancaria
 * 
 * @author cass465
 * @version 23/03/2018
 */
public abstract class Cuenta implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String tipoCuenta;
	private String nombreTitular;
	private int numeroCuenta;
	private long saldo;

	/**
	 * constructor de clase Cuenta
	 * 
	 * @param tipoCuenta
	 * @param nombreTitular
	 * @param numeroCuenta
	 * @param saldo
	 */
	public Cuenta(String tipoCuenta, String nombreTitular, int numeroCuenta, long saldo) {
		this.tipoCuenta = tipoCuenta;
		this.nombreTitular = nombreTitular;
		this.numeroCuenta = numeroCuenta;
		this.saldo = saldo;
	}

	abstract void retirar(long retirar);

	/**
	 * metodo para consignar
	 * 
	 * @param consignacion
	 */
	void consignar(long consignacion) {
		this.saldo += consignacion;
	}

	public String getTipoCuenta() {
		return tipoCuenta;
	}

	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}

	public String getNombreTitular() {
		return nombreTitular;
	}

	public void setNombreTitular(String nombreTitular) {
		this.nombreTitular = nombreTitular;
	}

	public int getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(int numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	public long getSaldo() {
		return saldo;
	}

	public void setSaldo(long saldo) {
		this.saldo = saldo;
	}

	@Override
	public String toString() {
		return tipoCuenta + " " + nombreTitular + " " + numeroCuenta;

	}
}
