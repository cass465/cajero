package logica;

/**
 * Cuenta de ahorros que hereda de Cuenta
 * 
 * @author cass465
 * @version 23/03/2018
 */
@SuppressWarnings("serial")
public class CuentaAhorros extends Cuenta {
	/**
	 * constructor de clase CuentaAhorros
	 * 
	 * @param tipoCuenta
	 * @param nombreTitular
	 * @param numeroCuenta
	 * @param saldo
	 */
	public CuentaAhorros(String tipoCuenta, String nombreTitular, int numeroCuenta, long saldo) {
		super(tipoCuenta, nombreTitular, numeroCuenta, saldo);
	}

	/**
	 * retirar en cuenta de ahorros
	 */
	@Override
	void retirar(long retiro) {
		long saldo = getSaldo();
		saldo -= retiro + (retiro * 0.05);
		System.out.println(
				"ESTIMADO USUARIO A SU SALDO SE LE RESTARA EL 5% DE SU RETIRO, ESE VALOR ES DE:" + (retiro * 0.05));
		setSaldo(saldo);
	}

}
