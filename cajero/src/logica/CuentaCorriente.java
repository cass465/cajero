package logica;

/**
 * Cuenta corriente que hereda de Cuenta
 * 
 * @author cass465
 * @version 23/03/2018
 */
@SuppressWarnings("serial")
public class CuentaCorriente extends Cuenta {

	public CuentaCorriente(String tipoCuenta, String nombreTitular, int numeroCuenta, long saldo) {
		super(tipoCuenta, nombreTitular, numeroCuenta, saldo);
		// TODO Auto-generated constructor stub
	}

	/**
	 * retirar en cuenta corriente
	 */
	@Override
	void retirar(long retiro) {
		long saldo = getSaldo();
		saldo -= retiro + (retiro * 0.02);
		System.out.println(
				"ESTIMADO USUARIO A SU SALDO SE LE RESTARA EL 2% DE SU RETIRO, ESE VALOR ES DE:" + (retiro * 0.02));
		setSaldo(saldo);

	}

}
