package principal;

import logica.Cajero;

/**
 * clase Main que ejecuta el programa
 * 
 * @author cass465
 * @version 23/03/2018
 */
public class Main {
	/**
	 * metodo main
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Cajero cajero = new Cajero();
		cajero.menuPrincipal();

	}

}
